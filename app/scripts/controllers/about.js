'use strict';

/**
 * @ngdoc function
 * @name authbeePageApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the authbeePageApp
 */
angular.module('authbeePageApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
