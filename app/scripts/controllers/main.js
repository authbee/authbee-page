'use strict';

/**
 * @ngdoc function
 * @name authbeePageApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the authbeePageApp
 */
angular.module('authbeePageApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
